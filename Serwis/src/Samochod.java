public class Samochod{
	
	public int cena, przebieg;
	public String marka, miasto;
	boolean nowy;
	
	

	public Samochod(int cena, String marka, String miasto, boolean nowy , int przebieg) {
		this.cena = cena; 
		this.marka = marka; 
		this.miasto = miasto;
		this.nowy = nowy;
		this.przebieg = przebieg;
		
	}


	public String getName() {
		return marka;
	}
	
	public String getMiasto() {
		return miasto;
	}
	
	public int getCena() {
		return cena;
	}
	
	public String getNowy(){
		
		if (nowy == true){
		return "tak";
		}
		else{ return "nie";}
	}

	public int getPrzebieg() {
		return przebieg;
	}


}